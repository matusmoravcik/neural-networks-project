# Projekt z Neuronovych siti

## Data
V projektu je vyuziti dataset 20 News Groups, ktery obsahuje texty, clanky, komentare 20 ruznych temat. Pro projekt byly vybrany pouze texty techto kategorii:

	* sci.electronics
	* sci.med
	* sci.space
	* sci.crypt

Z textu byly odstraneny metadata, hlavicky, paticky, texty byly zbaveny anglickych stop-words z baliku scikit-learn, lemmatizovany pomoci WordNetLemmatizer a byly ponechany pouze slova obsahujici pouze pismena. Take byly odstraneny slova, ktera se vyskytovala v mene nez 3 dokumentech z trenovaci mnoziny.

Uceni a optimalizace probiha na datech train.csv, vysledny vykon site je evaluovan na datech test.csv. Zadna informace z test.csv nebyla zavlecena do uceni a optimalizace a vysledky jsou tedy objektivni.

### Format dat
Na prvnim radku je header, kazdy sloupec znaci kolikrat se dane slovo v dokumentu vyskytlo. Sloupce target_class a target_name urcuji kategorie, do ktere dokument patri.
