/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matus
 */
public class DeepBeliefNetwork {

    private final List<RBM> layers = new ArrayList<>();

    private final static Logger log = Logger.getLogger(DeepBeliefNetwork.class.getName());

    /**
     *
     * @param structure number of neurons for each layer, e.g. 1000, 50, 50, 20
     */
    public DeepBeliefNetwork(final int... structure) {
        //check parameters
        if (structure.length < 2) {
            throw new IllegalArgumentException("The network must have at least two layers.");
        }

        //construct the network
        Integer last = structure[0];
        for (int i = 1; i < structure.length; ++i) {
            layers.add(new RBM(last, structure[i]));
            last = structure[i];
        }
    }

    /**
     * Pre-train deep belief network with RBM
     *
     * @param data Data set to train on.
     * @param learningRate Learning rate for contrastive divergence algorithm.
     * @param reconstructions Number of Gibbs sampling phases.
     * @param dropoutProbability Dropout probability for hidden neurons.
     * @param weightDecayRate Big weights penalty.
     * @param numEpochs Number of contrastive divergence training cycles.
     * @param skipLast Number of last RBMs which will be ignored during
     * pre-training.
     */
    public void preTrain(
            Data data,
            float learningRate,
            final int reconstructions,
            final float dropoutProbability,
            final float weightDecayRate,
            final int numEpochs,
            final int skipLast) {
        //check parameters        
        if (skipLast < 0) {
            throw new IllegalArgumentException("skipLast must be a positive number.");
        }

        //pre-train individual layers
        for (int i = 0; i < layers.size() - skipLast; ++i) {
            layers.get(i).contrastiveDivergence(data, learningRate, reconstructions, dropoutProbability, weightDecayRate, numEpochs);
            data = layers.get(i).feedForward(data);
        }
    }

    /**
     * Tune network with backpropagation algorithm.
     *
     * @param trainData Data set to train on.
     * @param learningRate Weight update modifier.
     * @param numEpochs Number of backpropagation cycles.
     */
    public void train(Data trainData, float learningRate, final int numEpochs) {
        //check parameters
        if (learningRate < 0) {
            throw new IllegalArgumentException("Learning rate must be a positive number.");
        }
        if (numEpochs < 1) {
            throw new IllegalArgumentException("At least one epoch is required.");
        }

        List<TrainingExample> data = trainData.getTrainingExamples();

        for (int epoch = 0; epoch < numEpochs; epoch++) {
            int exampleNumber = 0;
            float error = 0;
            for (TrainingExample example : data) {
                Vector[] activations = new Vector[layers.size() + 1];
                activations[0] = example.getSample();
                for (int layer = 0; layer < layers.size(); ++layer) {
                    activations[layer + 1] = layers.get(layer).feedForward(activations[layer].sigmoid());
                }
                error += example.getTarget().getRMSE(activations[layers.size()].sigmoid());

                // Propagate last layer
                Vector dEy = layers.get(layers.size() - 1).backpropagate(activations[layers.size() - 1],
                        activations[layers.size()], example.getTarget(), learningRate);

                // Propagate other layers
                for (int layer = 1; layer < layers.size(); layer++) {
                    RBM rbm = layers.get(layers.size() - layer - 1);
                    dEy = rbm.backpropagate(activations[layers.size() - layer - 1], activations[layers.size() - layer], dEy, learningRate);
                }
                exampleNumber++;
                log.log(Level.FINE, "Finished backpropagation on {0} sample.", exampleNumber);
            }
            log.log(Level.INFO, "Backpropagation epoch " + (epoch + 1) + " completed. Average RMSE: " + error / data.size());
        }
    }

    /**
     * Predicts targets of given set of vectors.
     *
     * @param predictSet Set of vectors to predict targets.
     * @return List of integers with predicted targets.
     */
    public List<Integer> predict(Data predictSet) {
        List<Integer> ret = new ArrayList<>(predictSet.getSize());
        int correct = 0;
        for (int i = 0; i < predictSet.getSize(); ++i) {
            Vector in = predictSet.getVectors().get(i);
            for (RBM rbm : layers) {
                in = rbm.feedForward(in).sigmoid();
            }

            float maxActivation = Float.MIN_VALUE;
            int result = -1;
            for (int j = 0; j < in.size; ++j) {
                if (in.get(j) > maxActivation) {
                    maxActivation = in.get(j);
                    result = j;
                }
            }
            ret.add(result);
        }
        double sucess = correct / (double) predictSet.getSize();
        return ret;
    }

    /**
     * Return accuracy of this network on test set.
     *
     * @param testSet Data to evaluate.
     * @deprecated use {@link #predict()} and ClassifierPerformanceMeasure
     * instance instead.
     * @return Accuracy in range <0,1>, where 1.0 is 100% accuracy.
     */
    @Deprecated
    public double evaluate(Data testSet) {
        int correct = 0;
        for (int i = 0; i < testSet.getSize(); ++i) {
            Vector in = testSet.getVectors().get(i);
            for (RBM rbm : layers) {
                in = rbm.feedForward(in).sigmoid();
            }

            float maxActivation = Float.MIN_VALUE;
            int result = -1;
            for (int j = 0; j < in.size; ++j) {
                if (in.get(j) > maxActivation) {
                    maxActivation = in.get(j);
                    result = j;
                }
            }
            log.log(Level.INFO, "expected: {0}, result: {1}", new Object[]{testSet.getTargetList().get(i), result});
            if (testSet.getTargetList().get(i) == result) {
                ++correct;
            }
        }
        double sucess = correct / (double) testSet.getSize();
        log.log(Level.INFO, "Evaluation summary: {0}/{1} samples were classified correctly ({2}%).", new Object[]{correct, testSet.getSize(), (Math.round(sucess * 10000) / 100f)});
        return sucess;
    }
}
