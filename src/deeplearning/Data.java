package deeplearning;

import com.opencsv.CSVReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Set;

/**
 *
 * @author Matus
 */
public final class Data {

    private final List<String> columns;
    private final List<Vector> inputList;
    private final List<Integer> targetList;
    private final List<String> targetNamesList;
    private final int numberOfTargets;

    private final static Logger log = Logger.getLogger(Data.class.getName());

    public class InvalidDataException extends Exception {

        public InvalidDataException(String message) {
            super(message);
        }
    }

    public Data(
            final List<String> columns,
            final List<Vector> inputList,
            final List<Integer> targetList,
            final List<String> targetNamesList,
            final int numberOfTargets) {
        this.columns = columns;
        this.inputList = inputList;
        this.targetList = targetList;
        this.targetNamesList = targetNamesList;
        this.numberOfTargets = numberOfTargets;
    }

    /**
     * Expects CSV with header and two last columns are class_integer,
     * class_string
     *
     * @param file CSV file name/path
     * @throws deeplearning.Data.InvalidDataException
     * @throws FileNotFoundException
     * @throws IOException
     */
    public Data(final String file) throws InvalidDataException, IOException {
        CSVReader reader = new CSVReader(new FileReader(file));
        String[] line = reader.readNext();

        columns = new ArrayList<>(Arrays.asList(line));

        // Delete target columns
        columns.remove(columns.size() - 1);
        columns.remove(columns.size() - 1);

        inputList = new ArrayList<>();
        targetList = new ArrayList<>();
        targetNamesList = new ArrayList<>();
        Set<Integer> targets = new HashSet<>();

        int lineNum = 0;
        line = reader.readNext();
        while (line != null) {
            Vector vector = new Vector(line.length - 2);
            for (int i = 0; i < columns.size(); ++i) {
                vector.set(i, Math.min(1, Float.parseFloat(line[i])));
            }
            inputList.add(vector);

            int target = Integer.parseInt(line[line.length - 2]);
            targetList.add(target);
            targets.add(target);
            targetNamesList.add(line[line.length - 1]);
            line = reader.readNext();

            //count lines
            lineNum++;
            if (lineNum % 100 == 0) {
                log.log(Level.FINE, "Imported {0} rows", lineNum);
            }
        }
        numberOfTargets = targets.size();
        log.log(Level.INFO, "Import DONE. {0} rows imported", lineNum);

        //check for anomalies
        checkData();
    }

    public List<Vector> getVectors() {
        return inputList;
    }

    public List<String> getColumns() {
        return columns;
    }

    public List<String> getTargetNamesList() {
        return targetNamesList;
    }

    public List<Integer> getTargetList() {
        return targetList;
    }

    public int getSize() {
        return inputList.size();
    }

    public int getDimension() {
        return inputList.get(0).getSize();
    }

    public int getCountOfTargets() {
        return numberOfTargets;
    }

    /**
     * Checks data set for anomalies, such as: -empty data set, -null vectors,
     * -inconsistent vector lengths, -non-numeric values, -values out of range
     * <0,1>.
     *
     * @throws deeplearning.Data.InvalidDataException
     */
    public void checkData() throws InvalidDataException {
        //check if empty
        if (inputList.isEmpty()) {
            throw new InvalidDataException("No data!");
        }

        //check for null vectors
        for (int i = 0; i < inputList.size(); ++i) {
            if (inputList.get(0) == null) {
                throw new InvalidDataException("Null vector at index " + i + ".");
            }
        }

        //check vector length consistency
        int length = inputList.get(0).getSize();
        for (int i = 0; i < inputList.size(); ++i) {
            if (length != inputList.get(i).getSize()) {
                throw new InvalidDataException("Vector lengths differ in size: vector[0]: " + length + ", vector[" + i + "]: " + inputList.get(i).getSize());
            }

            //check vector values
            for (int j = 0; j < inputList.get(i).getSize(); ++j) {
                //check non-numeric floats
                if (Float.isInfinite(inputList.get(i).get(j)) || Float.isNaN(inputList.get(i).get(j))) {
                    throw new InvalidDataException("Non-numeric float in vector[" + i + "] at index " + j + ".");
                }

                //check for floats outside of range <0,1>
                if (inputList.get(i).get(j) < 0 || inputList.get(i).get(j) > 1) {
                    throw new InvalidDataException("Float value outside of range <0,1> in vector[" + i + "] at index " + j + ".");
                }
            }
        }
    }

    public Data getSample(final int from, final int to) throws InvalidDataException {
        return new Data(columns, inputList.subList(from, to), targetList.subList(from, to), targetNamesList.subList(from, to), numberOfTargets);
    }

    public Data shuffle() {
        long seed = 5;
        Collections.shuffle(inputList, new java.util.Random(seed));
        Collections.shuffle(targetList, new java.util.Random(seed));
        Collections.shuffle(targetNamesList, new java.util.Random(seed));
        return this;
    }

    public List<TrainingExample> getTrainingExamples() {
        List<TrainingExample> list = new ArrayList<>();
        for (int i = 0; i < inputList.size(); ++i) {
            list.add(new TrainingExample(inputList.get(i), new TargetVector(targetList.get(i), numberOfTargets)));
        }
        return list;
    }

    public void mergeWith(Data dataObj) {
        inputList.addAll(dataObj.getVectors());
        targetList.addAll(dataObj.getTargetList());
        targetNamesList.addAll(dataObj.getTargetNamesList());
    }

    public void mergeWithCopy(Data otherDataObj) {
        mergeWith(otherDataObj.copy());
    }

    public Data copy() {
        return new Data(new ArrayList<>(columns), new ArrayList<>(inputList), new ArrayList<>(targetList), new ArrayList<>(targetNamesList), numberOfTargets);
    }

    public List<Data> split(int kFolds) {
        int dataLength = getSize();
        int nSamplesInFold = dataLength / kFolds;
        List<Data> ret = new ArrayList<>();
        int startIdx = 0;
        for (int endIdx = nSamplesInFold - 1; endIdx < dataLength; endIdx += nSamplesInFold) {
            Data fold = new Data(columns, inputList.subList(startIdx, endIdx + 1), targetList.subList(startIdx, endIdx + 1), targetNamesList.subList(startIdx, endIdx), numberOfTargets);
            ret.add(fold);
            startIdx = endIdx;
        }
        return ret;
    }
}
