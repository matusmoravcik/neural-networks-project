/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.List;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class DeepBeliefNetworkWrapper {

    private final DeepBeliefNetworkParameters par;
    private final DeepBeliefNetwork clf;

    public DeepBeliefNetworkWrapper(DeepBeliefNetworkParameters par) {
        this.par = par;
        this.clf = new DeepBeliefNetwork(par.getStructure());
    }

    public void train(Data trainData) {
        clf.preTrain(trainData, par.getPreTrainLearningRate(), par.getPreTrainReconstructions(), par.getPreTrainDropoutProbability(), par.getPreTrainWeightDecayRate(), par.getPreTrainNumEpochs(), par.getPreTrainSkipLast());
        clf.train(trainData, par.getTrainLearningRate(), par.getTrainNumEpochs());
    }

    public List<Integer> predict(Data predictData) {
        return clf.predict(predictData);
    }

}
