/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class GridSearch {

    private final static Logger log = Logger.getLogger(GridSearch.class.getName());
    private final DeepBeliefNetworkParameterGrid grid;
    private final CrossValidation cv;

    public GridSearch(DeepBeliefNetworkParameterGrid grid, CrossValidation cv) {
        log.log(Level.INFO, "GridSearch instance created.");
        this.grid = grid;
        if (!this.grid.isFilled()) {
            this.grid.fillParameterGrid();
        }
        this.cv = cv;
    }

    public DeepBeliefNetworkParameters getOptimalParameters() {
        log.log(Level.INFO, "Exhaustive grid searching for optimal parameters started with {0} parameter set variations.", grid.getNumVariations());
        float bestScore = -1;
        DeepBeliefNetworkParameters ret = null;
        int parameterNo = 0;
        for (DeepBeliefNetworkParameters param : grid) {
            parameterNo++;
            log.log(Level.INFO, "Evaluating parameter set no. {0}.", parameterNo);
            float currentScore = cv.evaluate(param);
            log.log(Level.INFO, "Results for network:\n{0}", param.toString());
            if (currentScore > bestScore) {
                bestScore = currentScore;
                ret = param;
                log.log(Level.INFO, "Found new best parameter set with score = {0}.", bestScore);
            } else {
                log.log(Level.INFO, "Checked parameter set has lower or equal score = {0} than the best score = {1}.", new Object[]{currentScore, bestScore});
            }
        }
        return ret;
    }
}
