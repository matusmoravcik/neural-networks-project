package deeplearning;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matus
 */
public class DeepLearning {

    private final static Logger log = Logger.getLogger(DeepLearning.class.getName());

    private static DeepBeliefNetworkParameterGrid getParamGrid(Data trData) {
        /**
         * SET TRAINING PARAMETERS HERE:
         */

        /*NETWORK STRUCTURE*/
        ArrayList<int[]> structure = new ArrayList<>();
        // too simple
        //structure.add(new int[]{trData.getDimension(), trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 3, trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 4, trData.getCountOfTargets()});
        // good
        //structure.add(new int[]{trData.getDimension(), 8, trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 11, trData.getCountOfTargets()});
        // ideal
        structure.add(new int[]{trData.getDimension(), 15, trData.getCountOfTargets()});
        // too complex
        //structure.add(new int[]{trData.getDimension(), 20, trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 15, 5, trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 15, 10, trData.getCountOfTargets()});
        //structure.add(new int[]{trData.getDimension(), 15, 10, trData.getCountOfTargets()});

        /*PRE-TRAIN PARAMETERS*/
        float[] preTrainLearningRateList = new float[]{0.01f}; //Learning rates for contrastive divergence algorithm.
        int[] preTrainReconstructionsList = new int[]{1}; //Numbers of Gibbs sampling phases.
        float[] preTrainDropoutProbabilityList = new float[]{0.2f}; //Dropout probabilities for hidden neurons.
        float[] preTrainWeightDecayRateList = new float[]{0.001f}; // Big weights penalties.
        int[] preTrainNumEpochsList = new int[]{50, 100}; //Numbers of contrastive divergence training cycles.
        int[] preTrainSkipLastList = new int[]{1, 2}; //Numbers of last RBMs which will be ignored during pre-training.

        /*TRAIN PARAMETERS*/
        float[] trainLearningRateList = new float[]{0.1f, 0.05f, 0.01f}; //Weight update modifiers.
        int[] trainNumEpochsList = new int[]{300}; //Numbers of backpropagation cycles.

        DeepBeliefNetworkParameterGrid grid = new DeepBeliefNetworkParameterGrid();
        grid.setStructureList(structure);
        grid.setPreTrainLearningRateList(preTrainLearningRateList);
        grid.setPreTrainReconstructionsList(preTrainReconstructionsList);
        grid.setPreTrainDropoutProbabilityList(preTrainDropoutProbabilityList);
        grid.setPreTrainWeightDecayRateList(preTrainWeightDecayRateList);
        grid.setPreTrainNumEpochsList(preTrainNumEpochsList);
        grid.setPreTrainSkipLastList(preTrainSkipLastList);
        grid.setTrainLearningRateList(trainLearningRateList);
        grid.setTrainNumEpochsList(trainNumEpochsList);
        grid.fillParameterGrid();
        return grid;
    }

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     * @throws deeplearning.Data.InvalidDataException
     */
    public static void main(String[] args) throws IOException, Data.InvalidDataException {

        log.setLevel(Level.INFO);
        //LOAD AND SPLIT DATA
        Data trainingDataSet = new Data("iris_train.csv").shuffle();
        
        Data testingDataSet = new Data("iris_test.csv").shuffle();
        CrossValidation cv = new CrossValidation(6, trainingDataSet, new Accuracy());

        //EXHAUSTIVE GRID SEARCH OF HYPERPARAMETERS
        DeepBeliefNetworkParameterGrid param = new DeepBeliefNetworkParameterGrid();
        GridSearch gs = new GridSearch(getParamGrid(trainingDataSet), cv);
        DeepBeliefNetworkParameters optimalParam = gs.getOptimalParameters();
        System.out.println("Grid search found optimal parameters: \n" + optimalParam);

        //TEST PERFORMANCE ON TEST DATA SET
        trainingDataSet = new Data("iris_train.csv").shuffle();
        DeepBeliefNetworkWrapper classifier = new DeepBeliefNetworkWrapper(optimalParam);
        classifier.train(trainingDataSet);
        List<Integer> predictedTargets = classifier.predict(testingDataSet);
        float accuracy = new Accuracy().eval(predictedTargets, testingDataSet.getTargetList());
        System.out.println("Accuracy on test set = " + accuracy + " %");

        writeConfusionMatrix(predictedTargets, testingDataSet.getTargetList(), "confusion_matrix.csv");

        Vector.shutdownExecutor();
    }

    private static void writeConfusionMatrix(List predictedTargets, List originalTargets, String csvPath) {
        int[][] confusionMatrix = new int[4][4];
        for (int[] confusionMatrixRow : confusionMatrix) {
            for (int predictedTargetIdx = 0; predictedTargetIdx < confusionMatrixRow.length; predictedTargetIdx++) {
                confusionMatrixRow[predictedTargetIdx] = 0;
            }
        }
        for (int i = 0; i < predictedTargets.size(); i++) {
            int actual = (int) originalTargets.get(i);
            int predicted = (int) predictedTargets.get(i);
            confusionMatrix[actual][predicted]++;
        }
        writeConfusionMatrix(confusionMatrix,csvPath);

    }

    private static void writeConfusionMatrix(int[][] confMtx, String csvPath) {
        List<String> lines = new ArrayList<String>();
        lines.add("Actual/Predicted, predicted 1, predicted 2, predicted 3, predicted 4");
        
        for (int r = 0; r < confMtx.length; r++) {
            String row = "actual "+(r+1)+", ";
            for (int c = 0; c < confMtx[r].length; c++) {
                row += Integer.toString(confMtx[r][c]);
                if (c < confMtx[r].length - 1) {
                    row += ", ";
                }
            }
            lines.add(row);
        }

        try {
            FileWriter writer = new FileWriter(csvPath);
            for (String row : lines) {
                writer.append(row + "\n");
            }
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
