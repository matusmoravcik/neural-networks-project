/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class DeepBeliefNetworkParameterGrid implements Iterable<DeepBeliefNetworkParameters> {

    private List<int[]> structureList; //network topologies

    //pre-train parameter lists
    private float[] preTrainLearningRateList; //Learning rates for contrastive divergence algorithm.
    private int[] preTrainReconstructionsList; //Numbers of Gibbs sampling phases.
    private float[] preTrainDropoutProbabilityList; //Dropout probabilities for hidden neurons.
    private float[] preTrainWeightDecayRateList; // Big weights penalties.
    private int[] preTrainNumEpochsList; //Numbers of contrastive divergence training cycles.
    private int[] preTrainSkipLastList; //Numbers of last RBMs which will be ignored during pre-training.

    //train parameter lists
    private float[] trainLearningRateList; //Weight update modifiers.
    private int[] trainNumEpochsList; //Numbers of backpropagation cycles.

    private final List<DeepBeliefNetworkParameters> parameterGrid;
    private boolean isFilled;
    private int nextIdx;

    public DeepBeliefNetworkParameterGrid() {
        parameterGrid = new ArrayList<>();
        isFilled = false;
        nextIdx = 0;
    }

    public void fillParameterGrid() {
        for (int[] structure : structureList) {
            for (float preTrainLearningRate : preTrainLearningRateList) {
                for (int preTrainReconstructions : preTrainReconstructionsList) {
                    for (float preTrainDropoutProbability : preTrainDropoutProbabilityList) {
                        for (float preTrainWeightDecayRate : preTrainWeightDecayRateList) {
                            for (int preTrainNumEpochs : preTrainNumEpochsList) {
                                for (int preTrainSkipLast : preTrainSkipLastList) {
                                    for (float trainLearningRate : trainLearningRateList) {
                                        for (int trainNumEpochs : trainNumEpochsList) {
                                            DeepBeliefNetworkParameters par = new DeepBeliefNetworkParameters();
                                            par.setStructure(structure);
                                            par.setPreTrainLearningRate(preTrainLearningRate);
                                            par.setPreTrainReconstructions(preTrainReconstructions);
                                            par.setPreTrainDropoutProbability(preTrainDropoutProbability);
                                            par.setPreTrainWeightDecayRate(preTrainWeightDecayRate);
                                            par.setPreTrainNumEpochs(preTrainNumEpochs);
                                            par.setPreTrainSkipLast(preTrainSkipLast);
                                            par.setTrainLearningRate(trainLearningRate);
                                            par.setTrainNumEpochs(trainNumEpochs);
                                            parameterGrid.add(par);
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
            }
        }
        isFilled = true;
    }

    public int getNumVariations() {
        return parameterGrid.size();
    }

    public boolean isFilled() {
        return isFilled;
    }

    public void setStructureList(List<int[]> structureList) {
        this.structureList = structureList;
    }

    public void setPreTrainLearningRateList(float[] preTrainLearningRateList) {
        this.preTrainLearningRateList = preTrainLearningRateList;
    }

    public void setPreTrainReconstructionsList(int[] preTrainReconstructionsList) {
        this.preTrainReconstructionsList = preTrainReconstructionsList;
    }

    public void setPreTrainDropoutProbabilityList(float[] preTrainDropoutProbabilityList) {
        this.preTrainDropoutProbabilityList = preTrainDropoutProbabilityList;
    }

    public void setPreTrainNumEpochsList(int[] preTrainNumEpochsList) {
        this.preTrainNumEpochsList = preTrainNumEpochsList;
    }

    public void setPreTrainSkipLastList(int[] preTrainSkipLastList) {
        this.preTrainSkipLastList = preTrainSkipLastList;
    }

    public void setTrainLearningRateList(float[] trainLearningRateList) {
        this.trainLearningRateList = trainLearningRateList;
    }

    public void setPreTrainWeightDecayRateList(float[] preTrainWeightDecayRateList) {
        this.preTrainWeightDecayRateList = preTrainWeightDecayRateList;
    }

    public void setTrainNumEpochsList(int[] trainNumEpochsList) {
        this.trainNumEpochsList = trainNumEpochsList;
    }

    @Override
    public Iterator iterator() {
        return (Iterator<DeepBeliefNetworkParameters>) parameterGrid.iterator();
    }

}
