package deeplearning;

/**
 * Created by jakac on 7.12.15.
 */
public class TargetVector extends Vector {

    public TargetVector(int target, int countOfTargets) {
        super(countOfTargets);
        for (int i = 0; i < countOfTargets; i++) {
            if (i == target) {
                values[i] = 1f;
            } else {
                values[i] = 0f;
            }
        }
    }

    public double getRMSE(Vector vector) {
        assert vector.size == size;

        double rmse = 0;
        for (int i = 0; i < size; i++) {
            rmse += Math.pow(vector.get(i) - values[i], 2);
        }
        return Math.sqrt(rmse / ((float) size));
    }
}
