package deeplearning;

import static deeplearning.Random.RANDOM;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Vector {
    
    public static int THREADS;
    private static ExecutorService EXECUTOR;

    protected final int size;
    protected final float[] values;

    public Vector(int size) {
        this.size = size;
        this.values = new float[size];
    }

    public Vector(float[] values) {
        this.size = values.length;
        this.values = values;
    }

    public int getSize() {
        return size;
    }

    public void set(int index, float value) {
        values[index] = value;
    }

    public float get(int index) {
        return values[index];
    }
    
    public static boolean initializeExecutor(int threads)
    {
        if(EXECUTOR != null && EXECUTOR.isTerminated())
        {
            return false;
        }
        if(threads <= 0)
        {
            throw new IllegalArgumentException("Cannot create fixed executor with zero or less threads.");
        }
        Vector.THREADS = threads;
        Vector.EXECUTOR = Executors.newFixedThreadPool(threads);
        return true;
    }
    
    public static void shutdownExecutor()
    {
        if(EXECUTOR != null)
        {
            EXECUTOR.shutdown();
            EXECUTOR = null;
        }
    }

    /**
     *
     * @param matrix
     * @param transposeMatrix
     * @return
     */
    public Vector dot(final Matrix matrix, final boolean transposeMatrix) {
        if (!transposeMatrix) {
            assert matrix.getNumRows() == size;
        } else {
            assert matrix.getNumCols() == size;
        }

        final int resultSize = (!transposeMatrix ? matrix.getNumCols() : matrix.getNumRows());

        // Resulting vector
        final Vector v = new Vector(resultSize);        
        
        if(EXECUTOR == null)
        {
            for (int c = 0; c < resultSize; ++c) { 
                // vector * column of matrix
                float result = 0;
                for (int i = 0; i < size; ++i) {
                    result += values[i]
                            * (!transposeMatrix ? matrix.get(i, c) : matrix.get(c, i));
                }
                v.set(c, result);
            }
        }
        else
        {
            final List<Callable<Boolean>> tasks = new ArrayList<>();        
            for(int i = 0; i < THREADS; ++i)
            {
                final int ii = i;
                tasks.add(new Callable<Boolean>()
                    {
                        @Override
                        public Boolean call() throws Exception {
                            for (int c = (ii * resultSize) / THREADS; c < ((ii + 1) * resultSize) / THREADS; ++c)
                            {
                                // vector * column of matrix
                                float result = 0;
                                for (int i = 0; i < size; ++i) {
                                    result += values[i]
                                            * (!transposeMatrix ? matrix.get(i, c) : matrix.get(c, i));
                                }
                                v.set(c, result);
                            }
                            return true;
                        }
                    });
            }
            try { 
                EXECUTOR.invokeAll(tasks);
            } catch (InterruptedException ex) {
                Logger.getLogger(Vector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return v;        
    }

    public interface MappingFunction {

        float map(float value);
    }

    /**
     *
     * @param mapping
     * @return
     */
    public Vector apply(MappingFunction mapping) {
        Vector v = new Vector(size);
        for (int i = 0; i < size; ++i) {
            v.set(i, mapping.map(get(i)));
        }
        return v;
    }

    /**
     * Initializes this vector with random values according to normal
     * distribution.
     *
     * @param multiplier Coefficient for obtained values.
     */
    public void initRandom(float multiplier) {
        for (int i = 0; i < size; ++i) {
            set(i, (float) (RANDOM.nextGaussian() * multiplier));
        }
    }

    /**
     * Initializes this vector with random values according to normal
     * distribution.
     */
    public void initRandom() {
        initRandom(1);
    }

    public void initZero() {
        for (int i = 0; i < size; ++i) {
            values[i] = 0f;
        }
    }

    /**
     * Dropouts random values of this vector by setting them to zero.
     *
     * @param probability Probability of dropout for each value of this vector.
     * @return Array of booleans, where result[i] == true iff the i-th value of
     * this vector was chosen for dropout (i.e. set to zero).
     */
    public boolean[] dropout(float probability) {
        boolean[] dropOuted = new boolean[size];

        for (int i = 0; i < size; ++i) {
            if (RANDOM.nextFloat() < probability) {
                values[i] = 0f;
                dropOuted[i] = true;
            } else {
                dropOuted[i] = false;
            }
        }
        return dropOuted;
    }

    /**
     * Dropouts values of this vector by setting them to zero according to given
     * dropout array.
     *
     * @param dropOuted Dropout array. A value on i-th index of this vector will
     * be set to zero iff dropOuted[i] == true.
     * @return Array of booleans, where result[i] == true iff the i-th value of
     * this vector was chosen for dropout (i.e. set to zero).
     */
    public boolean[] dropout(boolean[] dropOuted) {
        if (dropOuted == null) {
            return null;
        }

        assert dropOuted.length == size;

        for (int i = 0; i < size; i++) {
            if (dropOuted[i]) {
                set(i, 0f);
            }
        }

        return dropOuted;
    }

    public Vector plus(Vector other) {
        assert size == other.size;

        Vector v = new Vector(size);
        for (int i = 0; i < size; ++i) {
            v.set(i, get(i) + other.get(i));
        }

        return v;
    }

    public Vector minus(Vector other) {
        assert size == other.size;

        Vector v = new Vector(size);
        for (int i = 0; i < size; ++i) {
            v.set(i, get(i) - other.get(i));
        }

        return v;
    }

    public Vector multiply(Vector other) {
        assert size == other.size;

        Vector v = new Vector(size);
        for (int i = 0; i < size; ++i) {
            v.set(i, get(i) * other.get(i));
        }

        return v;
    }

    public Vector multiply(float other) {
        Vector v = new Vector(size);
        for (int i = 0; i < size; ++i) {
            v.set(i, get(i) * other);
        }

        return v;
    }

    public Vector sigmoid() {
        Vector ret = new Vector(size);
        for (int i = 0; i < size; ++i) {
            ret.set(i, (float) (1 / (1 + Math.pow(Math.E, -get(i)))));
        }
        return ret;
    }

    public Vector binomialSample() {
        Vector ret = new Vector(size);
        Vector sig = sigmoid();
        for (int i = 0; i < size; ++i) {
            ret.set(i, RANDOM.nextFloat() < sig.get(i) ? 1f : 0f);
        }
        return ret;
    }

    public Vector sigmoidGradient() {
        Vector ret = new Vector(size);
        Vector sig = sigmoid();
        for (int i = 0; i < size; ++i) {
            ret.set(i, sig.get(i) * (1 - sig.get(i)));
        }
        return ret;
    }
}
