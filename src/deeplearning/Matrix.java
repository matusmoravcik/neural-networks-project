package deeplearning;

import static deeplearning.Random.RANDOM;

/**
 * @author Matej Jakimov
 */
public class Matrix {

    private float[][] matrix;

    private int numRows;
    private int numCols;

    public Matrix(int rows, int cols) {
        numRows = rows;
        numCols = cols;
        matrix = new float[rows][cols];
    }

    /**
     * Andrew Ng recommends this: sqrt(6) / sqrt(N_in + N_out)
     */
    public void initRandom() {
        double epsilon = Math.sqrt(6f) / Math.sqrt(numRows + numCols);
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = (float) (RANDOM.nextGaussian() * epsilon);
            }
        }
    }

    public void initZero() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                matrix[i][j] = 0f;
            }
        }
    }

    public float get(int i, int j) {
        return matrix[i][j];
    }

    public void set(int i, int j, float value) {
        matrix[i][j] = value;
    }

    public int getNumRows() {
        return numRows;
    }

    public int getNumCols() {
        return numCols;
    }

    public void plus(Matrix other) {
        assert other.numRows == numRows && other.numCols == numCols;

        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                set(i, j, get(i, j) + other.get(i, j));
            }
        }
    }

}
