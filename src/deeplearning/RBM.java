/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matus
 */
public class RBM {

    private final int visibleLayerSize;
    private final int hiddenLayerSize;
    private final Matrix weightMatrix;
    private Vector hiddenBias;
    private Vector visibleBias;

    private final static Logger log = Logger.getLogger(RBM.class.getName());

    /**
     * Restricted Boltzmann Machine
     *
     * @param visibleLayerSize Number of visible neurons.
     * @param hiddenLayerSize Number of hidden neurons.
     */
    public RBM(final int visibleLayerSize, final int hiddenLayerSize) {
        if (visibleLayerSize < 1 || hiddenLayerSize < 1) {
            throw new IllegalArgumentException("Layer size cannot be less then 1");
        }

        this.visibleLayerSize = visibleLayerSize;
        this.hiddenLayerSize = hiddenLayerSize;
        weightMatrix = new Matrix(visibleLayerSize, hiddenLayerSize);
        weightMatrix.initRandom();
        hiddenBias = new Vector(hiddenLayerSize);
        hiddenBias.initRandom(0.2f);
        visibleBias = new Vector(visibleLayerSize);
        visibleBias.initRandom(0.2f);
    }

    /**
     * Computes potentials of hidden neurons according to the visible layer
     * (doesn't use an activation function!).
     *
     * @param visible Input to feed forward to.
     * @return Action potentials of hidden neurons.
     */
    public Vector feedForward(final Vector visible) {
        return visible.dot(weightMatrix, false).plus(hiddenBias);
    }

    /**
     * Transform Data input by feedForward == Compute hidden layer values. It
     * uses a standard sigmoid as an activation function, so it returns
     * activation probabilities (in <0,1>) instead.
     *
     * @param data input data set
     * @return Activation probabilities of the hidden layer.
     */
    public Data feedForward(final Data data) {
        List<Vector> transformed = new ArrayList<>();

        for (Vector sample : data.getVectors()) {
            transformed.add(feedForward(sample).sigmoid());
        }

        return new Data(null, transformed, data.getTargetList(), data.getTargetNamesList(), data.getCountOfTargets());
    }

    /**
     * Computes potentials of visible neurons according to the hidden layer
     * (doesn't use an activation function!).
     *
     * @param hidden Activations of hidden neurons.
     * @return Action potentials of visible neurons.
     */
    public Vector reconstruct(final Vector hidden) {
        return hidden.dot(weightMatrix, true).plus(visibleBias);
    }

    /**
     *
     * @param vector1
     * @param vector2
     * @return Mean squared error between given vectors.
     */
    public static float meanSquaredError(final Vector vector1, final Vector vector2) {
        assert vector1.getSize() == vector2.getSize();

        Vector dif = vector1.minus(vector2);
        float error = 0.0f;
        for (int i = 0; i < vector1.getSize(); ++i) {
            error += (float) Math.pow(dif.get(i), 2);
        }

        return error / ((float) vector1.size);
    }

    /**
     * Evaluates how well can this RBM reconstruct given input.
     *
     * @param sample Input vector.
     * @return Average mean squared reconstruction error for a neuron.
     */
    public float evaluate(final Vector sample) {
        return meanSquaredError(reconstruct(feedForward(sample).binomialSample()).sigmoid(), sample);
    }

    /**
     * Evaluates how well can this RBM reconstruct inputs in given data set.
     *
     * @param data Input data set.
     * @return Average mean squared reconstruction error for a neuron.
     */
    public float evaluate(final Data data) {
        float error = 0;
        int i = 0;
        for (Vector sample : data.getVectors()) {
            error += evaluate(sample);
            ++i;
            log.log(Level.FINE, "Evaluated " + i + " samples");
        }
        return error / data.getSize();
    }

    /**
     * Online training by contrastive divergence.
     *
     * @param sample Training example
     * @param learningRate Weight update modifier.
     * @param reconstructions Number of Gibbs sampling phases.
     * @param dropoutProbability Dropout probability for neurons in the hidden
     * layer.
     * @param weightDecayRate Big weights penalty.
     */
    public void contrastiveDivergence(
            final Vector sample,
            final float learningRate,
            final int reconstructions,
            final float dropoutProbability,
            final float weightDecayRate) {
        //check parameters
        if (learningRate < 0) {
            throw new IllegalArgumentException("Learning rate must be a positive number.");
        }
        if (reconstructions < 1) {
            throw new IllegalArgumentException("At least one reconstruction phase is required.");
        }
        if (dropoutProbability < 0 || dropoutProbability > 1) {
            throw new IllegalArgumentException("Dropout probability has to be in range <0,1>");
        }
        if (weightDecayRate < 0 || weightDecayRate > 1) {
            throw new IllegalArgumentException("Weight decay rate has to be in range <0,1>");
        }

        //prepare positive gradient
        Vector in = sample;
        Vector out = feedForward(sample).binomialSample(); //initialize hidden units

        //set dropout
        boolean[] dropOuted = null;
        if (dropoutProbability > 0) {
            dropOuted = out.dropout(dropoutProbability);
        }

        //Gibbs sampling
        Vector visible = in;
        Vector hidden = out;
        for (int i = 0; i < reconstructions; ++i) {
            visible = reconstruct(hidden).sigmoid();
            hidden = (i < reconstructions - 1) ? feedForward(visible).sigmoid() : feedForward(visible).binomialSample();

            hidden.dropout(dropOuted);
        }

        // weight adjusting
        for (int i = 0; i < visibleLayerSize; ++i) {
            for (int j = 0; j < hiddenLayerSize; ++j) {
                weightMatrix.set(i, j, weightMatrix.get(i, j)
                        + learningRate * (out.get(j) * in.get(i) - hidden.get(j) * visible.get(i)
                        - weightDecayRate * weightMatrix.get(i, j)));
            }
        }

        // adjusting biases
        for (int i = 0; i < visibleLayerSize; ++i) {
            visibleBias.set(i, visibleBias.get(i) + learningRate * (in.get(i) - visible.get(i)));
        }
        for (int i = 0; i < hiddenLayerSize; ++i) {
            hiddenBias.set(i, hiddenBias.get(i) + learningRate * (out.get(i) - hidden.get(i)));
        }
    }

    /**
     * Online training by contrastive divergence.
     *
     * @param data Data set to train on.
     * @param learningRate Weight update modifier.
     * @param reconstructions Number of Gibbs sampling phases.
     * @param dropoutProbability Dropout probability for neurons in the hidden
     * layer.
     * @param weightDecayRate Big weights penalty.
     */
    public void contrastiveDivergence(
            final Data data,
            final float learningRate,
            final int reconstructions,
            final float dropoutProbability,
            final float weightDecayRate) {
        int numSamples = 0;
        for (Vector sample : data.getVectors()) {
            RBM.this.contrastiveDivergence(sample, learningRate, reconstructions, dropoutProbability, weightDecayRate);
            numSamples++;
            log.log(Level.FINE, "Processed {0} sample", numSamples);
        }
    }

    /**
     * Online training by contrastive divergence.
     *
     * @param data Data set to train on.
     * @param learningRate Weight update modifier.
     * @param reconstructions Number of Gibbs sampling phases.
     * @param dropoutProbability Dropout probability for neurons in the hidden
     * layer.
     * @param weightDecayRate Big weights penalty.
     * @param numEpochs Number of training cycles.
     */
    public void contrastiveDivergence(
            final Data data,
            final float learningRate,
            final int reconstructions,
            final float dropoutProbability,
            final float weightDecayRate,
            final int numEpochs) {
        //check parameters        
        if (numEpochs < 1) {
            throw new IllegalArgumentException("At least one epoch is required.");
        }

        log.log(Level.INFO, "Initial average RMSE: " + evaluate(data));
        for (int epoch = 1; epoch <= numEpochs; epoch++) {
            RBM.this.contrastiveDivergence(data, learningRate, reconstructions, dropoutProbability, weightDecayRate);
            float result = evaluate(data);
            log.log(Level.INFO, "Pre-training epoch " + epoch + " completed. Average RMSE: " + result);
        }
    }

    /**
     * Backpropagation - Last layer of the network
     */
    public Vector backpropagate(Vector inputPotentials, Vector outputPotentials, TargetVector target, float learningRate) {
        assert outputPotentials.getSize() == target.getSize();

        Vector dEy = outputPotentials.sigmoid().minus(target);
        return backpropagate(inputPotentials, outputPotentials, dEy, learningRate);
    }

    /**
     * Backpropagation - Inner layer
     */
    public Vector backpropagate(Vector inputPotentials, Vector outputPotentials, Vector dEy, float learningRate) {
        assert outputPotentials.getSize() == dEy.getSize();
        assert outputPotentials.getSize() == weightMatrix.getNumCols();
        assert inputPotentials.getSize() == weightMatrix.getNumRows();

        Vector partJ = dEy.multiply(outputPotentials.sigmoidGradient());
        Vector partI = inputPotentials.sigmoid();

        for (int i = 0; i < weightMatrix.getNumRows(); ++i) {
            for (int j = 0; j < weightMatrix.getNumCols(); ++j) {
                weightMatrix.set(i, j, weightMatrix.get(i, j) - learningRate * partI.get(i) * partJ.get(j));
            }
        }

        hiddenBias = hiddenBias.minus(hiddenBias.multiply(partJ).multiply(learningRate));

        dEy = dEy.multiply(outputPotentials.sigmoidGradient());
        Vector dEyNew = new Vector(weightMatrix.getNumRows());
        for (int i = 0; i < dEyNew.getSize(); ++i) {
            float sum = 0;
            for (int j = 0; j < dEy.getSize(); ++j) {
                sum += dEy.get(j) * weightMatrix.get(i, j);
            }
            dEyNew.set(i, sum);
        }

        return dEyNew;
    }

    /*
     * Offline training on current given input set.
     * @param trainingSet Input set to train on.
     *
     public RBM trainBatch(final Data trainingSet)
     {
     //check parameters
     if(trainingSet == null)
     {
     throw new NullPointerException("Training set cannot be null.");
     }
     if(learningRate <= 0)
     {
     throw new IllegalArgumentException("Learning rate must be a positive number.");
     }        
     if(reconstructions <= 0)
     {
     throw new IllegalArgumentException("At least one reconstruction phase is required.");
     }
        
     //compute gradient sum
     Matrix gradientSum = new Matrix(visibleLayerSize, hiddenLayerSize);
     gradientSum.initZero();
     Vector visibleBiasSum = new Vector(visibleLayerSize);
     visibleBiasSum.initZero();
     Vector hiddenBiasSum = new Vector(hiddenLayerSize);
     hiddenBiasSum.initZero();
     int numSamples = 0;
     for(Vector sample : trainingSet.getVectors())
     {
     Vector in = sample;
     Vector out = feedForward(sample, true, true);
     Vector visible = sample;
     Vector hidden = out;

     //Gibbs sampling
     for(int i = 0; i < reconstructions; ++i)
     {
     visible = reconstruct(hidden);
     hidden = feedForward(visible, true, true);
     }  

     // accumulate gradient
     for(int i = 0; i < visibleLayerSize; ++i)
     {
     for(int j = 0; j < hiddenLayerSize; ++j)
     {
     gradientSum.set(i, j, weightMatrix.get(i, j) +
     learningRate * (out.get(j) * in.get(i) - hidden.get(j) * visible.get(i)));
     }
     }
     // Biases
     for(int i = 0; i < visibleLayerSize; i++) {
     visibleBiasSum.set(i, visibleBias.get(i) + in.get(i) - visible.get(i));
     }
     for(int i = 0; i < hiddenLayerSize; i++) {
     hiddenBiasSum.set(i, hiddenBias.get(i) + out.get(i) - hidden.get(i));
     }
     numSamples++;
     log.log(Level.INFO, "Processed " + numSamples + " sample");
     }
        
     // weight adjusting according to mean gradient
     weightMatrix.plus(gradientSum);
     visibleBias = visibleBias.plus(visibleBiasSum);
     hiddenBias = hiddenBias.plus(hiddenBiasSum);

     return this;
     }*/
}
