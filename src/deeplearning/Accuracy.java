/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.List;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class Accuracy implements ClassifierPerformanceMeasure {

    @Override
    public float eval(List predicted, List original) {
        int countAll = predicted.size();
        int correctPredictions = 0;
        for (int i = 0; i < predicted.size(); i++) {
            if (predicted.get(i) == original.get(i)) {
                correctPredictions++;
            }
        }
        return computeAccuracy(correctPredictions, countAll);
    }

    public float computeAccuracy(int n_correct, int n_all) {
        float ratio = ((float) n_correct / (float) n_all);
        return ratio * 100.0f;
    }

}
