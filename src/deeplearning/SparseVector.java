package deeplearning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static deeplearning.Random.RANDOM;

/**
 * Sparse vector implementation
 *
 * @author Matej Jakimov
 */
public class SparseVector {

    private int size;
    private Map<Integer, Float> values;

    public SparseVector(int size) {
        this.size = size;
        this.values = new HashMap<>();
    }

    public SparseVector(int size, Map<Integer, Float> values) {
        this(size);
        this.values = values;
    }

    public int getSize() {
        return size;
    }

    public void set(int index, float value) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Vector have not index " + Integer.toString(index));
        }

        if (Math.abs(value) > 0.000001) {
            values.put(index, value);
        } else if (values.containsKey(index)) {
            values.remove(index);
        }
    }

    public float get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Vector have not index " + Integer.toString(index));
        }

        if (values.containsKey(index)) {
            return values.get(index);
        } else {
            return 0f;
        }
    }

    public Vector dot(Matrix matrix, boolean transposeMatrix) {
        if (!transposeMatrix) {
            assert matrix.getNumRows() == size;
        } else {
            assert matrix.getNumCols() == size;
        }

        int resultSize = (!transposeMatrix ? matrix.getNumCols() : matrix.getNumRows());
        // Resulting vector
        Vector v = new Vector(resultSize);
        // Iterate over all columns of matrix, each will produce one float of vector v
        for (int c = 0; c < resultSize; c++) {
            // vector * column of matrix
            float result = 0;
            for (Map.Entry<Integer, Float> entry : values.entrySet()) {
                result += entry.getValue()
                        * (!transposeMatrix ? matrix.get(entry.getKey(), c) : matrix.get(c, entry.getKey()));
            }
            v.set(c, result);
        }

        return v;
    }

    public interface MappingFunction {

        float map(float value);
    }

    public Vector apply(MappingFunction mapping) {
        Vector v = new Vector(size);
        for (int i = 0; i < size; i++) {
            v.set(i, mapping.map(get(i)));
        }
        return v;
    }

    public void initRandom() {
        for (int i = 0; i < size; i++) {
            set(i, (float) (RANDOM.nextGaussian() * 0.2));
        }
    }

    public void initZero() {
        return;
    }

    public void dropout(float probability) {
        List<Integer> list = new ArrayList<>();
        for (int i : values.keySet()) {
            if (RANDOM.nextFloat() < probability) {
                list.add(i);
            }
        }
        for (int i : list) {
            set(i, 0f);
        }
    }

    public Vector plus(Vector other) {
        assert size == other.getSize();

        Vector v = new Vector(size);
        for (int i = 0; i < size; i++) {
            v.set(i, get(i) + other.get(i));
        }

        return v;
    }
}
