package deeplearning;

/**
 * Created by jakac on 7.12.15.
 */
public class TrainingExample {

    private Vector sample;

    private TargetVector target;

    public TrainingExample(Vector sample, TargetVector targetVector) {
        this.sample = sample;
        this.target = targetVector;
    }

    public Vector getSample() {
        return sample;
    }

    public TargetVector getTarget() {
        return target;
    }
}
