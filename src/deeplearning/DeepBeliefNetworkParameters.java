/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class DeepBeliefNetworkParameters {

    private int[] structure; //network topology

    //pre-train parameters
    private float preTrainLearningRate; //Learning rate for contrastive divergence algorithm.
    private int preTrainReconstructions; //Number of Gibbs sampling phases.
    private float preTrainDropoutProbability; //Dropout probability for hidden neurons.
    private float preTrainWeightDecayRate; // Big weights penalty.
    private int preTrainNumEpochs; //Number of contrastive divergence training cycles.
    private int preTrainSkipLast; //Number of last RBMs which will be ignored during pre-training.

    //train parameters
    private float trainLearningRate; //Weight update modifier.
    private int trainNumEpochs; //Number of backpropagation cycles.

    public int[] getStructure() {
        return structure;
    }

    public void setStructure(int[] structure) {
        this.structure = structure;
    }

    public float getPreTrainLearningRate() {
        return preTrainLearningRate;
    }

    public void setPreTrainLearningRate(float preTrainLearningRate) {
        this.preTrainLearningRate = preTrainLearningRate;
    }

    public int getPreTrainReconstructions() {
        return preTrainReconstructions;
    }

    public void setPreTrainReconstructions(int preTrainReconstructions) {
        this.preTrainReconstructions = preTrainReconstructions;
    }

    public float getPreTrainDropoutProbability() {
        return preTrainDropoutProbability;
    }

    public void setPreTrainDropoutProbability(float preTrainDropoutProbability) {
        this.preTrainDropoutProbability = preTrainDropoutProbability;
    }

    public int getPreTrainNumEpochs() {
        return preTrainNumEpochs;
    }

    public void setPreTrainNumEpochs(int preTrainNumEpochs) {
        this.preTrainNumEpochs = preTrainNumEpochs;
    }

    public int getPreTrainSkipLast() {
        return preTrainSkipLast;
    }

    public void setPreTrainSkipLast(int preTrainSkipLast) {
        this.preTrainSkipLast = preTrainSkipLast;
    }

    public float getTrainLearningRate() {
        return trainLearningRate;
    }

    public void setTrainLearningRate(float trainLearningRate) {
        this.trainLearningRate = trainLearningRate;
    }

    public int getTrainNumEpochs() {
        return trainNumEpochs;
    }

    public void setTrainNumEpochs(int trainNumEpochs) {
        this.trainNumEpochs = trainNumEpochs;
    }

    public float getPreTrainWeightDecayRate() {
        return preTrainWeightDecayRate;
    }

    public void setPreTrainWeightDecayRate(float weightDecayRate) {
        this.preTrainWeightDecayRate = weightDecayRate;
    }

    @Override
    public String toString() {
        String s = "";
        s += "***** DBN parameters list. *****\n";
        s += "structure: ";
        for (int i = 0; i < structure.length; i++) {
            if (i < structure.length - 1) {
                s += structure[i] + "-";
            } else {
                s += structure[i];
            }
        }
        s += "\n";
        s += "preTrainLearningRate: " + preTrainLearningRate + "\n";
        s += "preTrainReconstructions: " + preTrainReconstructions + "\n";
        s += "preTrainDropoutProbability: " + preTrainDropoutProbability + "\n";
        s += "preTrainNumEpochs: " + preTrainNumEpochs + "\n";
        s += "preTrainWeightDecayRate: " + preTrainWeightDecayRate + "\n";
        s += "preTrainSkipLast: " + preTrainSkipLast + "\n";
        s += "trainLearningRate: " + trainLearningRate + "\n";
        s += "trainNumEpochs: " + trainNumEpochs + "\n";
        return s;
    }

}
