/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deeplearning;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Matej Kvassay <www.matejkvassay.sk>
 */
public class CrossValidation {

    private final static Logger log = Logger.getLogger(CrossValidation.class.getName());
    List<Data> folds;
    ClassifierPerformanceMeasure perf;

    public CrossValidation(int kFolds, Data data, ClassifierPerformanceMeasure perf) {
        this.folds = data.split(kFolds);
        this.perf = perf;
        log.log(Level.INFO, "Cross validatior created with {0} folds each with size of {1}.", new Object[]{kFolds, this.folds.get(0).getSize()});
    }

    public float evaluate(DeepBeliefNetworkParameters param) {
        List<Float> scores = new ArrayList<>();
        for (int testIdx = 0; testIdx < folds.size(); testIdx++) {
            log.log(Level.INFO, "Evaluating using cross validation with test fold no. {0}.", testIdx + 1);
            Data trainData = null;
            Data testData = folds.get(testIdx);
            for (int foldIdx = 0; foldIdx < folds.size(); foldIdx++) {
                if (testIdx != foldIdx) {
                    if (trainData == null) {
                        trainData = folds.get(foldIdx).copy();
                    } else {
                        trainData.mergeWithCopy(folds.get(foldIdx));
                    }
                }
            }
            List<Integer> predictedTargets = trainAndPredict(param, trainData, testData);
            float score = perf.eval(predictedTargets, testData.getTargetList());
            scores.add(score);
        }
        return avg(scores);
    }

    private List<Integer> trainAndPredict(DeepBeliefNetworkParameters param, Data trainSet, Data testSet) {
        DeepBeliefNetworkWrapper clf = new DeepBeliefNetworkWrapper(param);
        clf.train(trainSet);
        return clf.predict(testSet);
    }

    public float avg(List<Float> array) {
        float sum = 0.0f;
        for (float val : array) {
            sum += val;
        }
        return sum / (float) array.size();
    }

}
